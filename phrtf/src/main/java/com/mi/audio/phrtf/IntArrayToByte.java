package com.mi.audio.phrtf;

import java.nio.ByteBuffer;

/**
 * @Author zhangsai
 * @CreateTime: 2024-11-21 18:14
 * @Description: TODO
 */
public class IntArrayToByte {

    public static byte[] convertIntArrayToByteArray(int[][] intArray) {
        // 计算总的字节数
        int rows = intArray.length;
        int cols = intArray[0].length;
        int byteCount = rows * cols * Integer.BYTES;

        // 创建ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(byteCount);

        // 填充ByteBuffer
        for (int[] row : intArray) {
            for (int value : row) {
                buffer.putInt(value);
            }
        }

        // 返回字节数组
        return buffer.array();
    }
}
