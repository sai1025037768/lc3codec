package com.mi.audio.phrtf;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangsai
 * @CreateTime: 2024-11-18 12:09
 * @Description: TODO
 */
public class AudioDetect {
    
    public static final int CHECK_FAIL = 0;
    public static final int CHECK_SUCCESS = 1;
    
    public static final int FRONT = 0x00;
    public static final int RIGHT_FRONT =0x01;
    public static final int LEFT_FRONT = 0x02;
    
    private Context mContext;
    
    static {
        System.loadLibrary("audio_detect");
    }
    
    
    private OnDetectResultListener onDetectListener;
    
    public AudioDetect(Context context){
        this.mContext = context;
    }
    
    public void init(){
        nativeInit();
    }

    /**
     * 
     * @param l1 左前方左耳
     * @param l2 左前方右耳
     * @param r1 右前方左耳
     * @param r2 右前方右耳
     * @param c1 前方左耳
     * @param c2 前方右耳
     */
    public void detect(byte[] l1, byte[] l2, byte[] r1, byte[] r2, byte[] c1, byte[] c2){

        String dir = mContext.getExternalFilesDir("audio_detect").getPath();
        String inputL1 = dir + "/inputL1";
        String inputL2 =  dir + "/inputL2";
        String inputR1 =  dir + "/inputR1";
        String inputR2 =  dir + "/inputR2";
        String inputC1 =  dir + "/inputC1";
        String inputC2 =  dir + "/inputC2";
        
        writeFile(inputL1, l1);
        writeFile(inputL2, l2);
        writeFile(inputR1, r1);
        writeFile(inputR2, r1);
        writeFile(inputC1, c1);
        writeFile(inputC2, c1);
        
       int checkResult = detectFile(inputL1, inputL2, inputR1, inputR2, inputC1, inputC2);

        if(checkResult == 111){
           int[][] hrtfData44k = getHrtfData44k();
           int inputShift44k = getInputShift44k();
           int[][] hrtfData48k = getHrtfData48k();
           int inputShift48k = getInputShift48k();
           
           
           if(onDetectListener != null) {
               byte[] bytes44k = IntArrayToByte.convertIntArrayToByteArray(hrtfData44k);
               byte[] bytes48k = IntArrayToByte.convertIntArrayToByteArray(hrtfData48k);
               onDetectListener.onDetectSuccess(CHECK_SUCCESS, bytes44k, inputShift44k, bytes48k, inputShift48k);
           }
       }else{
           if(onDetectListener != null) {
//               (111: all directions are available; 011: 45L is not available, 101: middle front direction is not available)
               List<Integer> directions = new ArrayList<>();
               if(checkResult == 0 || checkResult == 10 || checkResult == 100 || checkResult == 110){
                   directions.add(RIGHT_FRONT);
               }
               if(checkResult == 0 || checkResult == 1 || checkResult == 10 || checkResult == 11){
                   directions.add(LEFT_FRONT);
               }
               if(checkResult == 0 || checkResult == 1 || checkResult == 100 || checkResult == 101){
                   directions.add(FRONT);
               }
               onDetectListener.onDetectFail(CHECK_FAIL, directions);
           }
       }
       
    }

    private void writeFile(String fileName, byte[] bytes) {
        try {
            File file = new File(fileName);
            if(file.exists()){
                file.delete();
            }
            FileOutputStream stream = new FileOutputStream(file);
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void detect(String l1, String l2, String r1, String r2, String c1, String c2){
        int checkResult = detectFile(l1, l2, r1, r2, c1, c2);

        if(checkResult == 111){
            int[][] hrtfData44k = getHrtfData44k();
            int inputShift44k = getInputShift44k();
            int[][] hrtfData48k = getHrtfData48k();
            int inputShift48k = getInputShift48k();
            if(onDetectListener != null) {
                byte[] bytes44k = IntArrayToByte.convertIntArrayToByteArray(hrtfData44k);
                byte[] bytes48k = IntArrayToByte.convertIntArrayToByteArray(hrtfData48k);
                onDetectListener.onDetectSuccess(CHECK_SUCCESS, bytes44k, inputShift44k, bytes48k, inputShift48k);
            }
        }else{
            if(onDetectListener != null) {
                List<Integer> directions = new ArrayList<>();
                char[] chars = String.valueOf(checkResult).toCharArray();
                if(chars.length == 3){
                    if(chars[0] == '0'){
                        directions.add(LEFT_FRONT);
                    }
                    if(chars[1] == '0'){
                        directions.add(FRONT);
                    }
                    if(chars[2] == '0'){
                        directions.add(RIGHT_FRONT);
                    }
                }
                onDetectListener.onDetectFail(CHECK_FAIL, directions);
            }
        }
    }

    private native void nativeInit();
    
    private native int detectFile(String l1, String l2, String r1, String r2, String c1, String c2);
    
    private native int nativeDetect(byte[] l1, byte[] l2, byte[] r1, byte[] r2, byte[] c1, byte[] c2);
    
    private native int[][] getHrtfData44k();
    private native int getInputShift44k();

    private native int[][] getHrtfData48k();
    private native int getInputShift48k();
    
    private native int nativeTest(String l1, String l2,String r1,String r2, String c1,String c2);
    
    public void setOnDetectListener(OnDetectResultListener onDetectResultListener){
        this.onDetectListener = onDetectResultListener;
    }
    
    
    public interface OnDetectResultListener {
        
        void onDetectFail(int status, List<Integer> failDirections);
        void onDetectSuccess(int status, byte[] hrtfData44k, int inputShift44k, byte[] hrtfData48k, int inputShift48k);
    }
}
