#ifndef __SPATIAL_PHRTF_AUDIO_ALGO_H
#define __SPATIAL_PHRTF_AUDIO_ALGO_H
#include <stdlib.h>
#include <string.h>
#ifdef __cplusplus
extern "C"
{
#endif
/**
 * @brief phrtf structure initialization
 * @return phrtf initialized structure
 */
void* phrtf_init(void);

/**
 * @brief calculate matched index from hrtf dataset
 * @param *handle pointer which point to initialized phrtf structure
 * @param *x00 x45p x45n pointers which point to source signals (0 degree 45p degree 45n degree)
 * @param return matched index of hrtf data
 */
int phrtf_process(void* handle, float* x00, float* x45p, float* x45n);

/**
* @brief free phrtf structure
* @param* handle pointer which point to initialized phrtf structure
* @return{ * }
*/
void phrtf_free(void* handle);

/**
 * @brief allocateAndFill2DArray get hrtf data accroding to indexstamp
 * @param rows of 2-D hrtf array
 * @param cols of 2-D hrtf array
 * @param indexStamp index of matched hrtfdata
 * @param fs sampling frequency
 * @param inputShift hrtf inputShift
 * @return {*}
 */
int32_t** allocateAndFill2DArray(int rows, int cols, int indexStamp, int fs, int* inputShift);


/**
 * @brief hrtfWavCheck check the audio is available or not
 * @param *handle pointer which point to initialized phrtf structure
 * @param *wav point to original signal
 * @param return 1: available, 0 is useless(111: all directions are available; 011: 45L is not available, 101: middle front direction is not available)
 */
uint8_t hrtfWavCheck(void* handle, float* wav45pL, float* wav45pR, float* wav0L, float* wav0R, float* wav45nL, float* wav45nR);


int32_t** allocateAndFill2DArray(int rows, int cols, int indexStamp, int fs, int *inputShift);

void freeAndFill2DArray(int32_t** ptr,int rows);

#ifdef __cplusplus
}
#endif
#endif