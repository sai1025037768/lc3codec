package com.zs.media.lc3

import android.net.http.UrlRequest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mi.audio.lc3codec.LC3Decoder
import com.mi.audio.phrtf.AudioDetect
import com.zs.media.lc3.databinding.ActivityMainBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        binding.sampleText.text = "LC3"
        
        binding.sampleText.setOnClickListener {
            GlobalScope.launch {
                val input = getExternalFilesDir("input")!!.path +  "/byte_swapped.bin"
                val output = getExternalFilesDir("output")!!.path +  "/ra5_read_back.pcm"
                val decoder = LC3Decoder(120, 10000, 48000)
                decoder.create()
                
                val inStream = FileInputStream(input)
                val outStream = FileOutputStream(output)
               
                val allbytes = inStream.readAllBytes()
                val decode = decoder.decode(allbytes, allbytes.size)
                outStream.write(decode)
                inStream.close()
                outStream.close()

                decoder.destroy()
            }
          
        }
        
        binding.audioCheck.setOnClickListener {
            GlobalScope.launch {
                val input0L = getExternalFilesDir("input")!!.path +  "/0L.wav"
                val input0R = getExternalFilesDir("input")!!.path +  "/0R.wav"
                val inputNL = getExternalFilesDir("input")!!.path +  "/45nL.wav"
                val inputNR = getExternalFilesDir("input")!!.path +  "/45nR.wav"
                val inputPL = getExternalFilesDir("input")!!.path +  "/45pL.wav"
                val inputPR = getExternalFilesDir("input")!!.path +  "/45pR.wav"
                
                val detect = AudioDetect(applicationContext)
                detect.setOnDetectListener(object : AudioDetect.OnDetectResultListener{
                    override fun onDetectFail(status: Int, failDirections: MutableList<Int>?) {
                        
                    }

                    override fun onDetectSuccess(
                        status: Int,
                        hrtfData44k: Array<out IntArray>?,
                        inputShift44k: Int,
                        hrtfData48k: Array<out IntArray>?,
                        inputShift48k: Int
                    ) {
                    }

                })
                
                detect.init()

                val pl = File(inputPL).readBytes()
                val pr = File(inputPR).readBytes()
                val nl = File(inputNL).readBytes()
                val nr = File(inputNR).readBytes()
                val ol = File(input0L).readBytes()
                val or = File(input0R).readBytes()
//                detect.detect(pl, pr, nl, nr, ol, or)
                detect.detect(inputPL, inputPR, inputNL,inputNR, input0L,input0R)
            }
        }
    }


}